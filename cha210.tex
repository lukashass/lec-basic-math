\chapter{Vector Spaces}
\label{cha:cha210}

From school, the elements of $\RR^n$ are perhaps known as \enquote{vectors}.
In particular, $\RR^3$ is used a lot in physics,
because it can be used to model the space that we live in.
We will introduce vector spaces axiomatically here
and then make the connection to $\field^n$ for a field~$\field$.

\section{Fundamentals}

\begin{definition}
  Let $(\field, 0, 1, \oplus, \odot)$ be a field, $(V, +)$ a commutative group,
  and $\cdot \in \set{\field \times V \map V}$ an operation between elements of $\field$ and of $V$,
  yielding elements of $V$, that is, $\al \cdot v \in V$ for each $\al \in \field$ and each $v \in V$.
  Assume that all of the following conditions hold:
  \begin{IEEEeqnarray}{l"l}
    \label{eq:cha210:ax1} \al \cdot (v + w) = (\al \cdot v) + (\al \cdot w) & \forall \al \in \field \quad \forall v,w \in V \\
    \label{eq:cha210:ax2} (\al \oplus \bet) \cdot v = (\al \cdot v) + (\bet \cdot v) & \forall \al, \beta \in \field \quad \forall v \in V \\
    \label{eq:cha210:ax3} (\al \odot \bet) \cdot v = \al \cdot (\bet \cdot v) & \forall \al, \bet \in \field \quad \forall v \in V \\
    \label{eq:cha210:ax4} 1 \cdot v = v & \forall v \in V
  \end{IEEEeqnarray}
  Then $(V,+)$ is called a \term{vector space} over $\field$ with \term{scalar multiplication}~$\cdot$.
  The elements of $\field$ are called \term{scalars} and the elements of $V$ are called \term{vectors}.
  \qed
\end{definition}

We simplify notation.
Fields are denoted as in \autoref{cha:cha160}, namely $(\field, 0, 1, +, \cdot)$,
so we use the same symbol $+$ for the addition in $\field$ and the operation in $V$,
and we use the same symbol $\cdot$ for the multiplication in $\field$ and the scalar multiplication between $\field$ and~$V$.
Moreover, we usually omit $\cdot$ and only use it for typographical reasons.
The vector space axioms then read as follows:
\begin{IEEEeqnarray}{l"l}
  \label{eq:cha210:ax1:b} \al (v + w) = \al v + \al w & \forall \al \in \field \quad \forall v,w \in V \\
  \label{eq:cha210:ax2:b} (\al + \al) v = \al v + \bet v & \forall \al, \bet \in \field \quad \forall v \in V \\
  \label{eq:cha210:ax3:b} (\al \bet) v = \al (\bet v) & \forall \al, \bet \in \field \quad \forall v \in V \\
  \label{eq:cha210:ax4:b} 1 v = v & \forall v \in V
\end{IEEEeqnarray}

Due to~\eqref{eq:cha210:ax3:b} (or \eqref{eq:cha210:ax3}) we may write $\al\bet v$
for $\al,\bet \in \field$ and ${v \in V}$.
The neutral element of the group $(V,+)$ is called the \term{null vector}
and is denoted by~$0$, like the neutral element of addition in $\field$.
The simplest vector space is the \term{trivial vector space},
which consists of just a neutral element, that is, $V = \set{0}$.
The operation on $V$ and the scalar multiplication are obviously uniquely determined in this case.

\begin{remark}
  Let $\field$ be a field and $n \in \NN$.
  We endow $\field^n$ with entry\-/wise addition and entry\-/wise scalar multiplication,
  that is:
  \begin{IEEEeqnarray*}{l+l}
    (\eli{v}{n}) + (\eli{w}{n}) \df (v_1+w_1, \hdots, v_n+w_n) & \forall v,w \in \field^n \\
    \al \cdot (\eli{v}{n}) \df (\al v_1, \hdots, \al v_n) & \forall \al \in \field \quad \forall v \in \field^n
  \end{IEEEeqnarray*}
  With those operations, $\field^n$ is a vector space over $\field$.
  The neutral element is ${(0,\hdots,0) \in \field^n}$.
\end{remark}
\begin{proof}
  Follows easily from the field properties.
\end{proof}

\begin{definition}
  \label{def:cha210:matrix}
  Let $\field$ be a field and $d, n \in \NN$.
  A \term{matrix} $A$ with $d$ \term{rows} and $n$ \term{columns} over~$\field$,
  or a \term{$(d\times n)$~matrix} over $\field$, is a rectangular system of elements of~$\field$.
  That is, a total of $dn$ elements of $\field$ are arranged in rows and columns.
  We refer to the element in row~$i$ and column~$j$ by $A_{i,j}$ for each $i \in \setn{d}$ and each $j \in \setn{n}$,
  and call $A_{i,j}$ the $(i,j)$th \term{entry} or \term{component} of~$A$.
  The $(i,i)$th entries, $i \in \setn{\min\set{d,n}}$, are called the \term{diagonal entries}.
  The set of all such matrices is denoted by $\field^{d \times n}$.
  Formally, such a matrix is a function from $\setn{d} \times \setn{n}$ to $\field$,
  that is, $\field^{d \times n} = \fnset{\setn{d} \times \setn{n}}{\field}$.
  \qed
\end{definition}

For example, here is a $(3 \times 5)$ matrix over~$\RR$:
\begin{IEEEeqnarray}{c}
  \label{eq:cha210:ex-mat}
  A \df
  \begin{bmatrix}
    1 & 0 & 7 & 15 & 1 \\
    3 & 8 & 9 & 7 & 5 \\
    4 & 0 & 1 & 1 & 2
  \end{bmatrix}
\end{IEEEeqnarray}
In particular, $A_{1,3} = 7$, $A_{3,4} = 1$, and $A_{3,5} = 2$.

Matrices will be used extensively in later chapters.
Here, we introduce them mainly as another example of a vector space.

\begin{remark}
  Let $\field$ be a field and $d, n \in \NN$.
  We endow $\field^{d \times n}$ with entry\-/wise addition and entry\-/wise scalar multiplication,
  that is, for all $A,B \in \field^{d \times n}$ and all $\al \in \FF$,
  we define the matrices $A+B$ and $\al A$ by:
  \begin{IEEEeqnarray*}{l+l}
    (A+B)_{i,j} \df A_{i,j} + B_{i,j} & \forall i \in \setn{d} \quad \forall j \in \setn{n} \\
    (\al A)_{i,j} \df \al A_{i,j} & \forall i \in \setn{d} \quad \forall j \in \setn{n}
  \end{IEEEeqnarray*}
  With those operations, $\field^{d \times n}$ is a vector space over $\field$.
  The neutral element is the matrix where each entry is~$0$.
\end{remark}
\begin{proof}
  Follows easily from the definitions and the field properties.
\end{proof}

\begin{proposition}
  \label{prop:cha210:rules}
  Let $V$ be a vector space over a field $\field$.
  Then we have for all $\al \in \field$ and all $v \in V$:
  \begin{enumerate}
  \item\label{prop:cha210:rules:1} $0 v = 0$
  \item\label{prop:cha210:rules:2} $\al 0 = 0$
  \item\label{prop:cha210:rules:3} $\al v = 0 \iff \al = 0 \lor v = 0$
  \item\label{prop:cha210:rules:4} $-v = (-1) v$
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item
    We have:
    \begin{IEEEeqnarray*}{rCl"s}
      0 v & = & (0+0) v & $0$ is neutral element of addition in $\field$ \\
      & = & 0v + 0v & by \eqref{eq:cha210:ax2:b}
    \end{IEEEeqnarray*}
    Adding $-0v$, that is, the inverse of $0v$ in the group $(V,+)$,
    to both sides of this equation yields $0 = 0v$.
  \item
    We have:
    \begin{IEEEeqnarray*}{rCl"s}
      \al 0 & = & \al (0+0) & $0$ is neutral in $(V,+)$ \\
      & = & \al 0 + \al 0 & by \eqref{eq:cha210:ax1:b}
    \end{IEEEeqnarray*}
    Adding $-\al 0$ yields $0 = \al 0$.
  \item
    The implication $\impliedby$ is clear by~\ref*{prop:cha210:rules:1} and~\ref*{prop:cha210:rules:2}.
    Assume therefore $\al v = 0$ and $\al \neq 0$. We have to show $v=0$.
    We have:
    \begin{IEEEeqnarray*}{rCl"s}
      v & = & 1 v & by~\eqref{eq:cha210:ax4:b} \\
      & = & (\al^{-1} \al) v & since $\al \neq 0$ \\
      & = & \al^{-1} (\al v) & by \eqref{eq:cha210:ax3:b} \\
      & = & \al^{-1} 0 & by assumption \\
      & = & 0 & by \ref*{prop:cha210:rules:2}
    \end{IEEEeqnarray*}
  \item
    The statement means that $(-1) v$ is the inverse to $v$ in the group $(V,+)$.
    We have:
    \begin{IEEEeqnarray*}{rCl"s}
      v + (-1) v & = & 1 v + (-1) v & by \eqref{eq:cha210:ax4:b} \\
      & = & (1 + (-1)) v & by \eqref{eq:cha210:ax2:b} \\
      & = & 0 v & $-1$ is the additive inverse to $1$ in $\field$ \\
      & = & 0 & by \ref*{prop:cha210:rules:1}
    \end{IEEEeqnarray*}
    We have thus proved that $(-1) v$ is the inverse to $v$.
    \qedhere
  \end{enumerate}
\end{proof}

\section{Linear Combinations and Span}

\begin{definition}
  Let $V$ be a vector space over a field $\field$,
  and let $U \subseteq V$ be a finite set.
  \begin{itemize}
  \item Let $\lam \in \fnfield{U}$.
    We call $\sum_{u \in U} \lam_u u$ a \term{linear combination} (LC) of~$U$.
    For each $u \in U$, we call $\lam_u$ the \term{coefficient} of $u$ in the~LC.
    We write $\lam = 0$ to indicate when all of the coefficients are $0$;
    and $\lam \neq 0$ consequently means that there is at least one $u \in U$ with $\lam_u \neq 0$.
    When $U$ is given as an indexed set $U = \set{u_i}_{i \in I} \subseteq V$,
    then it often makes sense to use $I$ also as the domain for the coefficients,
    that is, we use $\lam \in \fnfield{I}$.
    This is true in particular when $U = \seti{u_i}{r} \subseteq V$ is an indexed set for some $r \in \NN$,
    in which case we can use~$\lam \in \field^r$.
    By convention, if $U = \emptyset$, then the only LC that $U$ can yield is the null vector~$0$.
  \item The set of all LC of $U$ is called the \term{span} of $U$ and is denoted by:
    \begin{IEEEeqnarray*}{c}
      \spann(U) \df \set[Big]{\sum_{u \in U} \lam_u u \suchthat \lam \in \fnfield{U}}
    \end{IEEEeqnarray*}
    In particular, the span of the empty set is $\spann(\emptyset) = \set{0}$,
    where $0$ is the null vector, that is, the neutral element of $V$.
    Instead of $\spann(\set{\eli{u}{r}})$ for some $\seti{u_i}{r} \subseteq V$
    we also write $\spann(\eli{u}{r})$.
  \item We call $U$ a \term{generating set} or \term{spanning set} for $V$ if $\spann(U) = V$.
    In particular, when we say that a set is a generating set, we imply that it is a finite set.
    \qed
  \end{itemize}
\end{definition}

\begin{example}
  \hfill\label{ex:cha210:LC}
  \begin{enumerate}
  \item
    Let us look at the following subset of $\RR^3$:
    \begin{IEEEeqnarray*}{c}
      U \df \set{\colvec{1 \\ 0 \\ 2}, \colvec{5 \\ 0 \\ 0}, \colvec{3 \\ 2 \\ 1}}
    \end{IEEEeqnarray*}
    An example of an LC of $U$ is:
    \begin{IEEEeqnarray*}{c}
      2 \colvec{1 \\ 0 \\ 2} + \colvec{5 \\ 0 \\ 0} + 10 \colvec{3 \\ 2 \\ 1} = \colvec{37 \\ 20 \\ 14}
    \end{IEEEeqnarray*}
    The coefficients are $2$, $1$, and $10$.
    Many other LC are conceivable.
  \item\label{ex:cha210:LC:2}
    As another example, we look at:
    \begin{IEEEeqnarray*}{c}
      U \df \set{\colvec{1 \\ 0 \\ 0}, \colvec{0 \\ 1 \\ 0}, \colvec{0 \\ 0 \\ 1}}
    \end{IEEEeqnarray*}
    An example of an LC of this $U$ is:
    \begin{IEEEeqnarray*}{c}
      7 \colvec{1 \\ 0 \\ 0} + 2 \colvec{0 \\ 1 \\ 0} + 4 \colvec{0 \\ 0 \\ 1} = \colvec{7 \\ 2 \\ 4}
    \end{IEEEeqnarray*}
    For general coefficients $\lam_1,\lam_2,\lam_3 \in \RR$, it looks like so:
    \begin{IEEEeqnarray*}{c}
      \lam_1 \colvec{1 \\ 0 \\ 0} + \lam_2 \colvec{0 \\ 1 \\ 0} + \lam_3 \colvec{0 \\ 0 \\ 1} = \colvec{\lam_1 \\ \lam_2 \\ \lam_3}
    \end{IEEEeqnarray*}
    By walking through all possible coefficients, we thus obtain all vectors in $\RR^3$,
    in other words, this $U$ is a generating set for~$\RR^3$.
  \item
    What we have seen in the previous item does not work with all set, of course.
    As an example, consider:
    \begin{IEEEeqnarray*}{c}
      U \df \set{\colvec{1 \\ 2 \\ 0}, \colvec{10 \\ 20 \\ 0}, \colvec{7 \\ 14 \\ 0}}
    \end{IEEEeqnarray*}
    For general coefficients $\lam_1,\lam_2,\lam_3 \in \RR$, we obtain the LC:
    \begin{IEEEeqnarray*}{c}
      \lam_1 \colvec{1 \\ 2 \\ 0} + \lam_2 \colvec{10 \\ 20 \\ 0} + \lam_3 \colvec{7 \\ 14 \\ 0}
      = \colvec{\lam_1 + 10 \lam_2 + 7 \lam_3 \\ 2 \lam_1 + 20 \lam_2 + 14 \lam_3 \\ 0}
      = \colvec{\lam_1 + 10 \lam_2 + 7 \lam_3 \\ 2 (\lam_1 + 10 \lam_2 + 7 \lam_3) \\ 0}
    \end{IEEEeqnarray*}
    Here we only get vectors of the form that the second entry is twice the first entry,
    and the last entry is zero.
    This is in spite of the fact that we have a set with three different vectors.
    Looking more closely, we obtain an idea why this is so:
    the second vector in the system is $10$ times the first one,
    and the third one is $7$ times the first one.
    This may be the reason that the span here is substantially sparser than the one obtained from the set in~\ref*{ex:cha210:LC:2}.
    In order to understand this in detail, we will introduce some more notions in the next section.\qed
  \end{enumerate}
\end{example}

\begin{proposition}
  \label{prop:cha210:xchange:span}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite.
  Let ${0 \neq \lam \in \fnfield{U}}$ and define $x \df \sum_{u \in U} \lam_u u$.
  Let $y \in U$ with $\lam_y \neq 0$ and define $U' \df U \setminus \set{y} \cup \set{x}$.
  Then $\spann(U) = \spann(U')$.
\end{proposition}
\begin{proof}
  First we note that
  $y = \frac{1}{\lam_y} x - \sum_{u \in U \setminus \set{y}} \frac{\lam_u}{\lam_y} u \in \spann(U')$.
  \par
  $\subseteq$)
  Let $v \in \spann(U)$, that is, $v = \sum_{u \in U} \mu_u u$ for some $\mu \in \fnfield{U}$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    v = \sum_{u \in U} \mu_u u = \mu_y y + \sum_{u \in U \setminus \set{y} } \mu_u u
  \end{IEEEeqnarray*}
  Since $y \in \spann(U')$ and $U \setminus \set{y} \subseteq U'$, this is an LC of $U'$.
  \par
  $\supseteq$)
  Let $v \in \spann(U')$ that is, $v = \sum_{u \in U'} \mu_u u$ for some $\mu \in \fnfield{U'}$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    v = \sum_{u \in U} \mu_u u = \mu_x x + \sum_{u \in U' \setminus \set{x} } \mu_u u
  \end{IEEEeqnarray*}
  Since $x \in \spann(U)$ and $U' \setminus \set{x} \subseteq U$, this is an LC of $U$.
\end{proof}

\section{Linear Independence}

\begin{definition}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite.
  We call $U$ \term{linearly independent} if:
  \begin{IEEEeqnarray*}{c}
    \forall \lam \in \fnfield{U} \holds \parens[Big]{ \sum_{u \in U} \lam_u u = 0 \implies \forall u \in U \holds \lam_u = 0 }
  \end{IEEEeqnarray*}
  If $U$ is \emphasis{not} linearly independent, we call $U$ \term{linearly dependent},
  but we will mostly avoid this term and use \enquote{\emphasis{not} linearly independent} instead.\qed
\end{definition}

If $U \neq \emptyset$, then linear independence of $U$ means
that only way to obtain the null vector as an LC of $U$ is in the trivial way of choosing all coefficients to zero.
The empty set is linearly independent,
since then the all quantifier \enquote{$\forall u \in U$} in the definition runs over an empty set.

\begin{example}
  \label{ex:cha210:lin-in}
  The set
  \begin{IEEEeqnarray*}{c}
    \set{%
      \colvec{1 \\ 1 \\ 1},
      \colvec{1 \\ 0 \\ 0},
      \colvec{0 \\ 1 \\ 0}}
  \end{IEEEeqnarray*}
  in $\RR^3$ is linearly independent.
  In order to prove this, let $\lam \in \RR^3$ such that:
  \begin{IEEEeqnarray*}{c}
    \lam_1
    \colvec{1 \\ 1 \\ 1}
    +
    \lam_2
    \colvec{1 \\ 0 \\ 0}
    +
    \lam_3
    \colvec{0 \\ 1 \\ 0}
    =
    \colvec{0 \\ 0 \\ 0}
  \end{IEEEeqnarray*}
  The last row implies $\lam_1 = 0$,
  with this in mind, the second row implies $\lam_3 = 0$,
  and finally the first row gives $\lam_2 = 0$.
  Hence all coefficients are zero.
  \qed
\end{example}

\begin{remark}
  Each subset of a linearly independent set is again linearly independent.
\end{remark}
\begin{proof}
  Easy exercise.
\end{proof}

\begin{proposition}
  \label{prop:cha210:one-is-LC}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite.
  Then $U$ is \emphasis{not} linearly independent if and only if
  there exists $x \in U$ such that $x \in \spann(U \setminus \set{x})$.
\end{proposition}
\begin{proof}
  $\implies$)
  By definition,
  there is $0 \neq \lam \in \fnfield{U}$
  such that $\sum_{u \in U} \lam_u u = 0$.
  Let $x \in U$ such that $\lam_x \neq 0$; such $x$ exists since $\lam \neq 0$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    x = - \sum_{u \in U \setminus \set{x}} \tfrac{\lam_u}{\lam_x} u
  \end{IEEEeqnarray*}
  So we succeeded in writing $x$ as an LC of $U \setminus \set{x}$.
  \par
  $\impliedby$)
  Let $x \in U$ such that $x \in \spann(U')$ with $U' \df U \setminus \set{x}$,
  say, $x = \sum_{u \in U'} \lam_u u$ for some $\lam \in \fnfield{U'}$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    x - \sum_{u \in U'} \lam_u u = 0
  \end{IEEEeqnarray*}
  On the left\-/hand side, there is an LC of $U$ yielding $0$,
  while the coefficient for $x$ is $1$ and not $0$.
  Hence $U$ is \emphasis{not} linearly independent.
\end{proof}

It is not true, however, that linear dependence of a set $U$ implies that each $u \in U$
is an LC of the other vectors in $U$.
As an example, consider:
\begin{IEEEeqnarray*}{c}
  U \df \set{\colvec{1 \\ 1}, \colvec{2 \\ 2}, \colvec{1 \\ 0}}
\end{IEEEeqnarray*}
This set is linearly independent since, for example, the second vector is a multiple of the first,
that is, it can be obtained by scalar multiplication of the first vector (by~$2$).
However, the third vector is no LC of the first two vectors.

The next proposition says that a vector being \enquote{the final straw},
meaning adding this vector transforms a set from being linearly independent to being \emphasis{not} linearly independent,
is always an LC of the vectors already in the set.

\begin{proposition}
  \label{prop:cha210:final-straw}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite and linearly independent.
  Let $x \in V$ be such that $U' \df U \cup \set{x}$ is \emphasis{not} linearly independent.
  Then, $x \in \spann(U)$.
\end{proposition}
\begin{proof}
  Since $U'$ is \emphasis{not} linearly independent, there is $0 \neq \lam \in \fnfield{U'}$ such that
  $\parens{\sum_{u \in U} \lam_u u} + \lam_{x} x = 0$.
  Assuming $\lam_{x} = 0$, then $\lam_u \neq 0$ for some $u \in U$,
  and hence we would have an LC of $U$ with result $0$ without all coefficients being $0$,
  which is impossible since~$U$ is linearly independent.
  Hence $\lam_x \neq 0$. It follows:
  \begin{IEEEeqnarray*}{c}
    x = - \sum_{u \in U} \tfrac{\lam_u}{\lam_x} u
  \end{IEEEeqnarray*}
  Thus $x$ is an LC of $U$, meaning $x \in \spann(U)$.
\end{proof}

A similar result is the following (see also \autoref{prop:cha210:xchange:span}).

\begin{proposition}
  \label{prop:cha210:xchange:lin-in}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite and linearly independent.
  Let ${0 \neq \lam \in \fnfield{U}}$ and define $x \df \sum_{u \in U} \lam_u u$.
  Let $y \in U$ with $\lam_y \neq 0$ and define $U' \df U \setminus \set{y} \cup \set{x}$.
  Then $U'$ is linearly independent.
\end{proposition}
\begin{proof}
  Let $\mu \in \fnfield{U'}$ such that $\sum_{u \in U'} \mu_u u = 0$.
  It follows:
  \begin{IEEEeqnarray*}{rCl}
    0 &=& \sum_{u \in U'} \mu_u u \\
    &=& \mu_x x + \sum_{u \in U \setminus \set{y}} \mu_u u \\
    &=& \mu_x \sum_{u \in U} \lam_u u + \sum_{u \in U \setminus \set{y}} \mu_u u \\
    &=& \mu_x \lam_y y + \sum_{u \in U \setminus \set{y}} (\mu_x \lam_u + \mu_u) u
  \end{IEEEeqnarray*}
  By linear independence of $U$, all those coefficients are $0$,
  in particular $\mu_x \lam_y = 0$, which by $\lam_y \neq 0$ implies $\mu_x = 0$.
  It follows that the coefficients in the last sum are in fact $\mu_x \lam_u + \mu_u = \mu_u$,
  and since those coefficients are $0$, it follows $\mu_u = 0$ for all $u \in U \setminus \set{y}$.
  We have thus proved the linear independence of~$U'$.
\end{proof}

\section{Subspaces}

\begin{definition}
  Let $V$ be a vector space over a field $\field$.
  A subset $S \subseteq V$ is called a \term{subspace} of $V$,
  denoted by $S \leq V$, if all of the following conditions hold:
  \begin{enumerate}
  \item $0 \in S$
  \item $v + w \in S$ for all $v,w \in S$
  \item $\al v \in S$ for all $\al \in \field$ and all $v \in S$\qed
  \end{enumerate}
\end{definition}

The last two conditions say that by adding vectors and by doing scalar multiplication,
we cannot leave $S$.
We also say that $S$ is \term{closed under addition and scalar multiplication}.

\begin{remark}
  \label{rem:cha210:span-in-S}
  Let $V$ be a vector space and $S \leq V$.
  Let $U \subseteq S$ be finite.
  Then each LC of $U$ is in $S$, that is, $\spann(U) \subseteq S$.
\end{remark}
\begin{proof}
  Follows by a trivial induction from the definition.
\end{proof}

The following remark implies that all notions and results
that we establish for vector spaces transfer to subspaces.

\begin{remark}
  \label{rem:cha210:subspace-is-vector-space}
  A subspace of a vector space over a field $\field$
  is itself a vector space over $\field$.
\end{remark}
\begin{proof}
  Follows directly from the definition.
\end{proof}

\begin{proposition}
  \label{prop:cha210:span-subspace}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite.
  Then $\spann(U) \leq V$.
  In particular, if $U \subseteq S$ for a subspace $S \leq V$, we have $\spann(U) \leq S$.
\end{proposition}
\begin{proof}
  Clearly, $0 \in \spann(U)$ since we obtain the null vector by choosing all coefficients to~$0$ in an LC.
  Now let $\al \in \field$ and $v, w \in \spann(U)$.
  By definition of the span, we find $\lam, \mu \in \fnfield{U}$ such that
  $v = \sum_{u \in U} \lam_u u$ and $w = \sum_{u \in U} \mu_u u$.
  It follows $v+w = \sum_{u \in U} (\lam_u+\mu_u) u$, hence $v+w$ is an LC of $U$, that is, $v+w \in \spann(U)$.
  Likewise, $\al v = \sum_{u \in U} \al \lam_u u$, hence $\al v \in \spann(U)$.
  \par
  The statement about the subspace $S$ follows with \autoref{rem:cha210:subspace-is-vector-space}.
\end{proof}

\begin{definition}
  For any set $S \subseteq V$ and $a \in V$ we write $a + S \df \set{a + v \suchthat v \in S}$.
  If $S \leq V$ then $a+S$ is called an \term{affine subspace}.\qed
\end{definition}

The next proposition states that if we shift a subspace in a \enquote{direction already contained in it},
then the subspace remains as it is.

\begin{proposition}
  \label{prop:cha210:contained-direction}
  Let $V$ be a vector space over a field $\field$, and $U \subseteq V$ be finite.
  Let $a \in V$ and $v \in \spann(U)$.
  Then $(a+v) + \spann(U) = a + \spann(U)$.
\end{proposition}
\begin{proof}
  By assumption, there is $\mu \in \fnfield{U}$ such that $v = \sum_{u \in U} \mu_u u$.
  We prove both inclusions.
  \par
  $\subseteq$)
  Let $x \in (a+v) + \spann(U)$.
  Then there is $\lam \in \fnfield{U}$ such that $x = a + v + \sum_{u \in U} \lam_u u$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    x
    = a + v + \sum_{u \in U} \lam_u u
    = a + \sum_{u \in U} \mu_u u + \sum_{u \in U} \lam_u u
    = a + \sum_{u \in U} (\mu_u+\lam_u) u
  \end{IEEEeqnarray*}
  Hence $x \in a + \spann(U)$.
  \par
  $\supseteq$) Let $x \in a + \spann(U)$.
  Then there is $\lam \in \fnfield{U}$ such that $x = a + \sum_{u \in U} \lam_u u$.
  It follows:
  \begin{IEEEeqnarray*}{rCl}
    x
    &=& a + \sum_{u \in U} \lam_u u \\
    &=& a + \sum_{u \in U} (\lam_u-\mu_u+\mu_u) u \\
    &=& a + \sum_{u \in U} \mu_u u + \sum_{u \in U} (\lam_u-\mu_u) u \\
    &=& a + v + \underbrace{\sum_{u \in U} (\lam_u-\mu_u) u}_{\in \spann(U)}
  \end{IEEEeqnarray*}
  Hence $x \in (a+v) + \spann(U)$.
\end{proof}

\section{Basis and Dimension}

\begin{definition}
  Let $V$ be a vector space over a field $\field$, and $B \subseteq V$ be finite.
  We call $B$ a \term{basis} of $V$ if the following two conditions hold:
  \begin{enumerate}
  \item $B$ is linearly independent.
  \item $B$ is a generating set for $V$, that is, $\spann(B) = V$.
    \qed
  \end{enumerate}
\end{definition}

The concept can be extended to the non\-/finite case, but we will not consider this here.
For us, a basis is always a finite set.

\begin{remark}
  Let $n \in \NN$.
  A basis of the vector space $\field^n$ is the \term{standard basis} $\seti{e_i}{n}$,
  defined by
  \begin{IEEEeqnarray*}{c}
    (e_i)_j =
    \begin{cases}
      1 & i = j \\
      0 & \text{otherwise}
    \end{cases}
  \end{IEEEeqnarray*}
  for each $i, j \in \setn{n}$.
  We refer it is as the \term{standard basis of length $n$}.
  The case $n=3$ is treated in \autoref{ex:cha210:LC}~\ref{ex:cha210:LC:2}.
\end{remark}
\begin{proof}
  Linear independence is clear (can be shown by similar arguments as in \autoref{ex:cha210:lin-in}).
  For the generating property, let $v \in \field^n$.
  Then $v = \sum_{i=1}^n v_i e_i$, that is, we use $v$ as the coefficients for an LC,
  which yields $v$.
\end{proof}

\begin{proposition}
  \label{prop:cha210:basis1}
  Let $V$ be a vector space over a field $\field$, and $B \subseteq V$ be finite.
  Then the following two conditions are equivalent:
  \begin{enumerate}
  \item\label{prop:cha210:basis1:1}
    $B$ is a basis of $V$.
  \item\label{prop:cha210:basis1:2}
    Each element of $V$ can be written as an LC of $B$ with uniquely determined coefficients.
  \end{enumerate}
\end{proposition}
\begin{proof}
  $\implies$)
  Let $x \in V$.
  Since $B$ as a basis in particular spans $V$,
  we find ${\lam \in \fnfield{B}}$ such that ${\sum_{b \in B} \lam_b b = x}$.
  In order to prove that the coefficients are uniquely determined, let ${\mu \in \fnfield{B}}$ be such that $\sum_{b \in B} \mu_b b = x$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    0 = x - x = \sum_{b \in B} \lam_b b - \sum_{b \in B} \mu_b b = \sum_{b \in B} (\lam_b-\mu_b) b
  \end{IEEEeqnarray*}
  Since $B$ is linearly independent, it follows $\lam_b-\mu_b=0$
  for all $b \in B$, hence $\lam_b=\mu_b$.
  The coefficients of the two LC are hence the same.
  \par
  $\impliedby$)
  The generating set property is clear.
  We prove that $B$ is linearly independent.
  To this end let $\lam \in \fnfield{B}$ such that $\sum_{b \in B} \lam_b b = 0$.
  One possible choice for $\lam$ is $\lam = 0$, that is:
  \begin{IEEEeqnarray}{c}
    \label{eq:prop:cha210:basis1:1}
    \lam_b = 0 \quad \forall b \in B
  \end{IEEEeqnarray}
  This is also the only possibility since otherwise the coefficients for an LC of $B$ yielding a particular vector
  (the null vector of $V$ in this case) would not be uniquely determined.
  Hence~\eqref{eq:prop:cha210:basis1:1} holds, which implies linear independence.
\end{proof}

\begin{proposition}
  \label{prop:cha210:basis-equiv}
  Let $V$ be a vector space over a field $\field$ and $B \subseteq V$ be finite.
  The following conditions are equivalent:
  \begin{enumerate}
  \item\label{prop:cha210:basis-equiv:1} $B$ is a basis of $V$.
  \item\label{prop:cha210:basis-equiv:2} $B$ is a generating set of $V$,
    and for each $x \in B$, the set $B \setminus \set{x}$ is no generating set of $V$.
  \item\label{prop:cha210:basis-equiv:3} $B$ is linearly independent,
    and for each $v \in V \setminus B$,
    the set $B \cup \set{v}$ is \emphasis{not} linearly independent.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \impref{prop:cha210:basis-equiv:1}{prop:cha210:basis-equiv:2}
  The generating set property is part of the definition of a basis.
  Now let $x \in B$.
  If $x \in \spann(B \setminus \set{x})$,
  then $B$ would \emphasis{not} be linearly independent by \autoref{prop:cha210:one-is-LC}.
  Hence $x$ is missed by the span of $B \setminus \set{x}$, so the latter is no generating set.
  \par
  \impref{prop:cha210:basis-equiv:2}{prop:cha210:basis-equiv:1}
  We have to show that $B$ is linearly independent.
  Let $\lam \in \fnfield{B}$ such that $\sum_{b \in B} \lam_b b = 0$.
  For contradiction, assume there is $x \in B$ with $\lam_x \neq 0$ and denote $B' \df B \setminus \set{x}$.
  We have $x = - \sum_{b \in B'} \frac{\lam_b}{\lam_x} b \in \spann(B')$.
  Now let $v \in V$.
  Since $B$ is a generating set, we have $v = \sum_{b \in B} \mu_b b$ for some $\mu \in \fnfield{B}$.
  It follows:
  \begin{IEEEeqnarray*}{c}
    v = \sum_{b \in B} \mu_b b = \mu_x x + \sum_{b \in B'} \mu_b b \in \spann(B')
  \end{IEEEeqnarray*}
  Since $v$ was arbitrary, $B'$ is a generating set; a contradiction.
  \par
  \impref{prop:cha210:basis-equiv:1}{prop:cha210:basis-equiv:3}
  Follows from \autoref{prop:cha210:one-is-LC}, since $x \in \spann(B)$.
  \par
  \impref{prop:cha210:basis-equiv:3}{prop:cha210:basis-equiv:1}
  We have to show that $B$ is a generating set.
  Let $v \in V$.
  Since $B' \df B \cup \set{v}$ is \emphasis{not} linearly independent,
  we find $0 \neq \lam \in \fnfield{B'}$ with $\sum_{b \in B'} \lam_b b = 0$.
  Since $B$ is linearly dependent, we can rule out $\lam_v = 0$.
  Hence $v = - \sum_{b \in B' \setminus \set{v}} \frac{\lam_b}{\lam_v} b$.
  Since $B' \setminus \set{v} = B$, this means $v \in \spann(B)$.
\end{proof}

The next three propositions show how to (theoretically) obtain a basis
under different assumptions.
Their proofs are very similar to each other.

\begin{proposition}
  \label{prop:cha210:iter-1}
  Let $V$ be a vector space over a field~$\field$,
  and let $U \subseteq V$ be a generating set (in particular finite).
  Then $V$ admits a basis $B \subseteq U$.
\end{proposition}
\begin{proof}
  Denote $B_0 \df \emptyset$.
  We perform the following procedure for ${i = 0,1,2,\hdots}$.
  If there is a vector $b_{i+1} \in U \setminus \spann(B_{i})$,
  then define $B_{i+1} \df B_{i} \cup \set{b_{i+1}}$ and continue;
  otherwise deliver $B \df B_i$ as the result.
  \par
  By \autoref{prop:cha210:final-straw}, all the sets that we create in this process are linearly independent.
  At the same time, since $U \subseteq \spann(B)$ and $\spann(U) = V$, we have $\spann(B) = V$.
  Hence $B$ is a basis of~$V$.
\end{proof}

\begin{proposition}
  \label{prop:cha210:iter-2}
  Let $V$ be a finite\-/dimensional vector space over a field~$\field$,
  and let $U \subseteq V$ be finite and linearly independent.
  Then there is a basis $B$ of $V$ with $U \subseteq B$.
\end{proposition}
\begin{proof}
  Denote $B_0 \df U$.
  We perform the following procedure for ${i = 0,1,2,\hdots}$.
  If there is a vector $b_{i+1} \in V \setminus \spann(B_{i})$,
  then define $B_{i+1} \df B_{i} \cup \set{b_{i+1}}$ and continue;
  otherwise deliver $B \df B_i$ as the result.
  \par
  Since $U$ is linearly independent
  and by \autoref{prop:cha210:final-straw}, all the sets that we create in this process are linearly independent.
  At the same time, by construction, $B$ is a generating set of $V$.
  Hence $B$ is a basis of~$V$.
\end{proof}

\begin{proposition}
  \label{prop:cha210:iter-3}
  Let $V$ be a vector space over a field~$\field$.
  Assume that there is $n \in \NN$ such that $\card{U} \leq n$ for all linear independent sets $U \subseteq V$.
  Then $V$ admits a basis $B$ with $\card{B} \leq n$.
\end{proposition}
\begin{proof}
  Denote $B_0 \df \emptyset$.
  We perform the following procedure for ${i = 0,1,2,\hdots}$.
  If there is a vector $b_{i+1} \in V \setminus \spann(B_{i})$,
  then define $B_{i+1} \df B_{i} \cup \set{b_{i+1}}$ and continue;
  otherwise deliver $B \df B_i$ as the result.
  \par
  By \autoref{prop:cha210:final-straw}, all the sets that we create in this process are linearly independent.
  Hence, by assumption, this procedure stops after at most $n$ iterations,
  since there is no linearly independent set of size more than $n$ in this vector space.
  At the same time, this procedure guarantees that $U$ is a generating set.
  Hence $U$ is a basis of~$V$.
\end{proof}

\begin{theorem}[Steinitz Exchange Lemma]
  \label{thm:cha210:steinitz}
  Let $B \subseteq V$ be a basis of a vector space $V$ over a field $\field$.
  Let $U \subseteq V$ be finite and linearly independent.
  Then there is $B_0 \subseteq B$ such that:
  \begin{enumerate}
  \item $\card{B_0} = \card{U}$, in particular $\card{U} \leq \card{B}$.
  \item $(B \setminus B_0) \cup U$ is a basis of $V$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  We do induction on $\card{U}$.
  \par
  Induction base: $\card{U} = 0$.
  Then the choice of $B_0 \df \emptyset$ is fine.
  \par
  Induction step: Assume that the theorem holds for all linearly independent $U'$ in place of $U$, whenever $\card{U'} < \card{U}$.
  We pick $x \in U$ arbitrarily and denote $U' \df U \setminus \set{x}$.
  Then $U'$ is linearly independent and $\card{U'} < \card{U}$.
  Hence by the induction hypothesis, we find $B_0' \subseteq B$ such that $\card{B'_0} = \card{U'}$
  and $B' \df (B \setminus B'_0) \cup U'$ is a basis of $V$.
  \par
  We turn our attention to the vector $x$ that we removed from $U$ in order to obtain~$U'$.
  There is $\lam \in \fnfield{B'}$ such that $x = \sum_{b \in B'} \lam_b b$.
  If $\lam_b = 0$ for all $b \in B \setminus B'_0$, then $x \in \spann(U')$,
  which by \autoref{prop:cha210:one-is-LC} is a contradiction to $U$ being linearly independent.
  Hence there is $y \in B \setminus B'_0 \subseteq B'$ with $\lam_y \neq 0$.
  Define $B_0 \df B_0' \cup \set{y}$.
  Then $B_0 \subseteq B$, $\card{B_0} = \card{U}$, and $B^* \df (B \setminus B_0) \cup U =  B' \setminus \set{y} \cup \set{x}$.
  By \autoref{prop:cha210:xchange:span}, we have $\spann(B^*) = \spann(B') = V$, hence $B^*$ is a generating set.
  By \autoref{prop:cha210:xchange:lin-in}, $B^*$ is linearly independent.
  Hence $B^*$ is a basis.
\end{proof}

\begin{corollary}
  Let $B_1$ and $B_2$ each be a basis of the same vector space $V$ over a field $\field$.
  Then $\card{B_1} = \card{B_2}$.
\end{corollary}
\begin{proof}
  Since a basis in particular is linearly independent,
  the previous theorem implies $\card{B_1} \leq \card{B_2}$
  and $\card{B_2} \leq \card{B_1}$.
\end{proof}

\begin{definition}
  Let $V$ be a vector space over a field $\field$ that admits a basis.
  Such a vector space is also called \term{finitely generated} or \term{finite\-/dimensional}.
  The common cardinality of all the bases of $V$ is called the \term{dimension} of $V$
  and is denoted by~$\dim(V)$.\qed
\end{definition}

\begin{remark}
  \begin{enumerate}
  \item The trivial vector space $\set{0}$ has dimension~$0$.
  \item For each $n \in \NN$ the vector space $\field^n$ over $\field$ has dimension $n$.
  \item Each field is a vector space of dimension $1$ over itself.
  \end{enumerate}
\end{remark}
\begin{proof}
  It is easy to see that:
  \begin{enumerate}
  \item $\emptyset$ is a basis of $\set{0}$;
  \item the standard basis of length $n$ is a basis of $\field^n$;
  \item $\set{1}$ is a basis of the field over itself.\qedhere
  \end{enumerate}
\end{proof}

\begin{remark}
  Let $V$ be a vector space over a field $\field$,
  and let $U \subseteq V$ be finite and linearly independent.
  Then $W \df \spann(U)$ has dimension $\card{U}$.
\end{remark}
\begin{proof}
  Follows since $U$ is a basis of $W$.
\end{proof}

\begin{proposition}
  \label{prop:cha210:card-n}
  Let $V$ be a vector space of dimension $n \in \NN$ over a field $\field$.
  Then every linearly independent subset of cardinality $n$ is a basis of $V$.
\end{proposition}
\begin{proof}
  Let $U \subseteq V$ be linearly independent with $\card{U} = n$.
  If there was ${v \in V \setminus \spann(U)}$,
  by \autoref{prop:cha210:final-straw}, $U \cup \set{v}$ would be linearly independent,
  that is, a linearly independent set of cardinality $n+1$.
  This is a contradiction to \autoref{thm:cha210:steinitz}.
\end{proof}

\begin{theorem}
  \label{thm:cha210:sub-dim}
  Let $V$ be a vector space of dimension $n \in \NN$ over a field $\field$ and $S \leq V$.
  Then:
  \begin{enumerate}
  \item\label{thm:cha210:sub-dim:1} $S$ is finite\-/dimensional and $\dim(S) \leq n$.
  \item\label{thm:cha210:sub-dim:2} $S = V$ if and only if $\dim(S) = n$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \begin{enumerate}
  \item By \autoref{thm:cha210:steinitz}, each linearly independent subset of $V$
    has cardinality at most $n$.
    This clearly also holds for linearly independent subsets of $S$.
    We apply \autoref{prop:cha210:iter-3} to the vector space $S$ over $\field$,
    which gives us a basis $B$ of $S$ with $\card{B} \leq n$.
    (Note that for some finite set $U \subseteq S$,
    linear independence in the vector space $S$ is the same as linear independence in the vector space~$V$.)
  \item The $\implies$ direction is trivial.
    The other follows from \autoref{prop:cha210:card-n}
    and \autoref{rem:cha210:span-in-S}.
    \qedhere
  \end{enumerate}
\end{proof}

%%% Local Variables:
%%% TeX-master: "BasicMath.tex"
%%% End:
