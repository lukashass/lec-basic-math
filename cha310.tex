\chapter{Convergence and Continuity}
\label{cha:cha310}

Although this part is on real analysis,
we introduce convergence and continuity in a more general context.
This is in order for us to recognize the essentials of these notions.

\section{Normed and Metric Spaces}

\begin{definition}
  Let $V$ be a vector space over a field $\field \in \set{\RR, \CC}$,
  and let ${\norm{\cdot} \in \fnset{V}{\RRnn}}$ be a function.
  We call $\norm{\cdot}$ a \term{norm} on $V$
  and $(V, \norm{\cdot})$ a \term{normed vector space} if all of the following conditions hold:
  \begin{enumerate}
  \item $\norm{x} = 0 \iff x = 0$ for all $x \in V$
  \item $\norm{\al x} = \abs{\al} \norm{x}$ for all $\al \in \field$ and all $x \in V$
  \item $\norm{x+y} \leq \norm{x} + \norm{y}$ for all $x,y \in V$ (triangle inequality)
    \qed
  \end{enumerate}
\end{definition}
\par\smallskip

\begin{remark}
  \label{rem:cha310:abs-is-norm}
  The absolute value as defined in \autoref{def:cha160:abs} is a norm
  on $\RR$ and on $\CC$, considered as vector spaces over themselves.
\end{remark}
\begin{proof}
  Follows directly from \autoref{prop:cha160:complex-rules}.
\end{proof}

\begin{definition}
  Let $\field \in \set{\RR, \CC}$ and $n \in \NN$.
  We define the \term{scalar product} between two vectors $x,y \in \field^n$ by:
  \begin{IEEEeqnarray*}{c+x*}
    x \bigcdot y \df \sum_{i=1}^n x_i \overline{y_i} & \qedarray{1}
  \end{IEEEeqnarray*}
\end{definition}

Note that the scalar product is a different concept than the scalar multiplication
that we know from vector spaces.
The scalar product maps two vectors to a scalar,
while the scalar multiplication maps a scalar and a vector to a vector.

\begin{proposition}
  \label{prop:cha310:scalar}
  Let $\field \in \set{\RR, \CC}$ and $n \in \NN$, and let $\al \in \field$ and $x, y, z \in \field^n$.
  Then:
  \begin{enumerate}
  \item\label{prop:cha310:scalar:1} $x \bigcdot y = \overline{y \bigcdot x}$
  \item\label{prop:cha310:scalar:2} If $\field = \RR$, then $x \bigcdot y = y \bigcdot x$.
  \item\label{prop:cha310:scalar:3} $x \bigcdot x \in \RRnn$
  \item\label{prop:cha310:scalar:4} $x \bigcdot x = 0 \iff x = 0$
  \item\label{prop:cha310:scalar:5} $(\al x) \bigcdot y = \al \cdot (x \bigcdot y)$ and $x \bigcdot (\al y) = \overline{\al} \cdot (x \bigcdot y)$
  \item\label{prop:cha310:scalar:6} $(x+y) \bigcdot z = x \bigcdot z + y \bigcdot z$
  \end{enumerate}
\end{proposition}
\par\smallskip

\begin{definition}
  Let $\field \in \set{\RR, \CC}$ and $n \in \NN$.
  The following function on $\field^n$ is called the \term{Euclidean norm}:
  \begin{IEEEeqnarray*}{c+x*}
    \norm{x}_2 \df \sqrt{x \bigcdot x} = \sqrt{\sum_{i=1}^n \abs{x_i}^2}
    &\qedarray{1}
  \end{IEEEeqnarray*}
\end{definition}

By \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:3},
the square root is defined.
The second formulation follows directly from the definition.
In a minute, we will prove that it is indeed a norm.
It is obvious that the first norm property follows from
\autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:4}
and \autoref{rem:cha160:power-root-inverse}, and we will use it from now on.

\begin{theorem}[Cauchy-Schwarz inequality]
  Let $\field \in \set{\RR, \CC}$ and $n \in \NN$, and let $x, y \in \field^n$.
  Then $\abs{x \bigcdot y} \leq \norm{x}_2 \cdot \norm{y}_2$.
\end{theorem}
\begin{proof}
  If $y=0$, the claim is trivial (both sides are zero), so assume $y \neq 0$,
  which implies $y \bigcdot y > 0$.
  For each $\al \in \field$, we have:
  \begin{IEEEeqnarray*}{rCl"s}
    0 &\leq& (x - \al y) \bigcdot (x - \al y) & \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:3} \\
    &=& x \bigcdot (x - \al y) - \al \cdot (y \bigcdot (x - \al y)) & \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:5} and \ref{prop:cha310:scalar:6} \\
    &=& x \bigcdot x - \overline{\al} \cdot (x \cdot y) - \al \cdot (y \bigcdot x - \overline{\al} \cdot (y \bigcdot y))
    & \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:5} and \ref{prop:cha310:scalar:6} \\
    &=& x \bigcdot x - \overline{\al} \cdot (x \cdot y) - \al \cdot (y \bigcdot x) + \al \overline{\al} \cdot (y \bigcdot y)
  \end{IEEEeqnarray*}
  Exploiting this for $\al \df \frac{x \bigcdot y}{y \bigcdot y}$ yields:
  \begin{IEEEeqnarray*}{rCl}
    0 &\leq& x \bigcdot x - \frac{y \bigcdot x}{y \bigcdot y} \cdot (x \bigcdot y)
    - \frac{x \bigcdot y}{y \bigcdot y} \cdot (y \bigcdot x)
    + \frac{x \bigcdot y}{y \bigcdot y} \cdot \frac{y \bigcdot x}{y \bigcdot y} \cdot (y \bigcdot y) \\
    &=& x \bigcdot x - \frac{x \bigcdot y}{y \bigcdot y} \cdot (y \bigcdot x)
    = x \bigcdot x - \frac{x \bigcdot y}{y \bigcdot y} \cdot \overline{x \bigcdot y}
    = \norm{x}_2^2 - \frac{\abs{x \bigcdot y}^2}{\norm{y}_2^2}
  \end{IEEEeqnarray*}
  This implies the claimed inequality.
\end{proof}

In the preceding proof, we never used the actual definition of the scalar product
but merely the properties asserted by \autoref{prop:cha310:scalar}.
Hence the Cauchy-Schwarz inequality also holds in a more general context (in so\=/called \term{Pre\=/Hilbert spaces}),
which we do not consider here.

\begin{proposition}
  Let $\field \in \set{\RR, \CC}$ and $n \in \NN$.
  Then the \term{Euclidean norm} is a norm on $\field^n$.
\end{proposition}
\begin{proof}
  It only remains to check the second and third norm property.
  \begin{enumerate}[start=2]
  \item We have:
    \begin{IEEEeqnarray*}{rCl"s}
      \norm{\al x}_2 &=& \sqrt{(\al x) \bigcdot (\al x)} \\
      &=& \sqrt{\al \cdot (x \bigcdot (\al x))} & by \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:5} \\
      &=& \sqrt{\al \cdot \overline{\al} \cdot (x \bigcdot x)} & by \autoref{prop:cha310:scalar}~\ref{prop:cha310:scalar:5} \\
      &=& \sqrt{\abs{\al}^2 \cdot (x \bigcdot x)} \\
      &=& \abs{\al} \cdot \norm{x}_2
    \end{IEEEeqnarray*}
  \item Exercise. (First show $\norm{x + y}_2 = \norm{x}_2 + \norm{y}_2 + 2 \cdot \Re (x \bigcdot y)$
    and then use the Cauchy-Schwarz inequality.)\qedhere
  \end{enumerate}
\end{proof}
\par\smallskip

In fact, it can be shown that $\norm{x}_p \df \sqrt[p]{\sum_{i=1}^n \abs{x_i}^p}$
is a norm on $\field^n$ for each $p \in \NN$, with $\field \in \set{\RR, \CC}$.
The first two norm properties are easy to prove,
but the triangle inequality requires some effort.
We do not go into this here.
Instead we move on to metric spaces.

\begin{definition}
  Let $M$ be a non\-/empty set and $d \in \fnset{M \times M}{\RRnn}$.
  We call $d$ a \term{metric} on $M$ and $(M,d)$ a \term{metric space}
  if all of the following conditions hold:
  \begin{enumerate}
  \item $d(x,y) = 0 \iff x = y$ for all $x,y \in M$
  \item $d(x,y) = d(y,x)$ for all $x,y \in M$ (symmetry)
  \item $d(x,y) \leq d(x,z) + d(z,y)$ for all $x,y,z \in M$ (triangle inequality)
    \qed
  \end{enumerate}
\end{definition}
\par\smallskip

\begin{remark}
  Let $(V, \norm{\cdot})$ be a \term{normed vector space}.
  Then $d(x,y) \df \norm{x-y}$ for each $x,y \in V$ defines a metric on $V$.
\end{remark}
\begin{proof}
  Follows directly from the definition, the vector space properties, and the norm properties.
\end{proof}

\begin{remark}
  \begin{enumerate}
  \item Let $M$ be a non\-/empty set and define
    \begin{IEEEeqnarray*}{c}
      d(x,y) \df
      \begin{cases}
        0 & \text{if $x=y$} \\
        1 & \text{otherwise}
      \end{cases}
    \end{IEEEeqnarray*}
    for each $x,y \in M$.
    Then $(M,d)$ is a metric space, and $d$ is called the \term{discrete metric} on $M$.
  \item Let $(V,\norm{\cdot})$ be a normed vector space over a field $\field \in \set{\RR,\CC}$.
    Define
    \begin{IEEEeqnarray*}{c}
      d(x,y) \df
      \begin{cases}
        \norm{x-y} & \text{if exists $\lam \in \fieldnz$ with $x = \lam y$} \\
        \norm{x} + \norm{y} & \text{otherwise}
      \end{cases}
    \end{IEEEeqnarray*}
    for each $x,y \in V$.
    Then $(V,d)$ is a metric space, and $d$ is called the \term{French Metro metric}.
  \end{enumerate}
\end{remark}
\begin{proof}
  \begin{enumerate}
  \item Follows easily from the definitions.
  \item Exercise.\qedhere
  \end{enumerate}
\end{proof}

We use the notation \enquote{$\hdots > 0$} instead of \enquote{$\hdots \in \RRpos$} from now on,
for example, $\veps > 0$ to say $\veps \in \RRpos$.

\begin{definition}
  Let $(M,d)$ be a metric space.
  \begin{enumerate}
  \item Let $x \in M$ and $\veps > 0$.
    We define the \term{$\veps$\=/ball} around $x$, or the ball with radius $\veps$ around $x$, by:
    \begin{IEEEeqnarray*}{c}
      B_\veps(x) \df \set{ y \in M \suchthat d(x,y) < \veps }
    \end{IEEEeqnarray*}
  \item A set $X \subseteq M$ is called \term{open} if for each $x \in X$,
    there is $\veps > 0$ such that $B_\veps(x) \subseteq X$.
  \item A set $X \subseteq M$ is called \term{closed} if $M \setminus X$ is open.\qed
  \end{enumerate}
\end{definition}

When we talk about $\veps$\=/balls, open, and closed sets
in the context of $\RR$ or $\CC$,
we always mean this with respect to the metric $d(x,y) \df \abs{x-y}$ for each $x,y \in \CC$.

\begin{remark}
  \label{rem:ch310:clopen}
  Let $(M,d)$ be a metric space.
  \begin{itemize}
  \item Then the sets $\emptyset$ and $M$ each are open and closed.
  \item For each $\veps > 0$ and each $x \in M$, the ball $B_\veps(x)$ is open.
  \end{itemize}
\end{remark}
\begin{proof}
  Follows directly from the definitions.
\end{proof}

\begin{proposition}
  \label{prop:cha310:open-closed-op}
  \begin{enumerate}
  \item Unions of open sets are open and joins of finitely many open sets are open.
  \item Intersections of closed sets are closed and intersections of finitely many closed sets are closed.
  \end{enumerate}
\end{proposition}
\begin{proof}
\end{proof}

\begin{remark}
  Let $a, b \in \RR \cup \set{-\infty, \infty}$ with $a \leq b$.
  Then:
  \begin{enumerate}
  \item $\intoo{a}{b}$ is open;
  \item $\intcc{a}{b}$ is closed if $a,b \in \RR$.
  \end{enumerate}
\end{remark}
\begin{proof}
  \begin{enumerate}
  \item The case $a=b$ is clear, since $\intoo{a}{b} = \emptyset$ is open then by \autoref{rem:ch310:clopen}.
    We first consider the case that $a,b \in \RR$.
    So let $a < b$ and $x \in \intoo{a}{b}$.
    Define $\veps \df \min\set{x-a, b-x}$, which yields $\veps > 0$.
    We prove $B_\veps(x) \subseteq \intoo{a}{b}$.
    To this end, let $y \in B_\veps(x)$.
    We only treat the case $x \leq y$, since the other one works likewise.
    We have $a < x \leq y$ on the one hand,
    and on the other hand $y - x < \veps \leq b-x$, hence $y < b$.
    It follows $a < x < b$, that is, $x \in \intoo{a}{b}$.
    \par
    Now if $a = -\infty$ and $b \in \RR$, a similar proof with $\veps \df b-x$ works.
    For $a \in \RR$ and $b \in \infty$, we use $\veps \df x-a$.
    If $a= -\infty$ and $b= \infty$, the statement follows from \autoref{rem:ch310:clopen}.
  \item We have $\RR \setminus \intcc{a}{b} = \intoo{{-\infty}}{a} \cup \intoo{b}{\infty}$.
    Hence the claim follows from the previous item and \autoref{prop:cha310:open-closed-op}.
    \qedhere
  \end{enumerate}
\end{proof}

\section{Convergence}

\begin{definition}
  Let $(M,d)$ be a metric space, and let $x \in \fnset{\NN}{M}$ be a sequence in $M$ and $\xi \in M$.
  Then we call $\xi$ a \term{limit} of $x$ if the following holds:
  \begin{IEEEeqnarray*}{c}
    \forall \veps > 0 \innerholds \exists n_0 \in \NN \innerholds \forall n \in \NN \holds n \geq n_0 \implies x_n \in B_\veps(\xi)
  \end{IEEEeqnarray*}
  A sequence that has a limit is called \term{convergent}.\qed
\end{definition}

The condition is also written shortly as:
\begin{IEEEeqnarray*}{c}
  \forall \veps > 0 \innerholds \exists n_0 \in \NN \innerholds \forall n \geq n_0 \holds x_n \in B_\veps(\xi)
\end{IEEEeqnarray*}

\begin{remark}
  A convergent sequence has exactly one limit.
\end{remark}
\begin{proof}
  So let $(M,d)$ be a metric space, and let $x \in \fnset{\NN}{M}$ be a convergent sequence in $M$.
  Let $\xi, \xi' \in M$ be limits of $x$, and for contradiction assume $\xi \neq \xi'$.
  Then $\veps \df d(\xi,\xi') > 0$ by the properties of a metric, and $\frac{\veps}{2} > 0$.
  By the definition of a metric, there is $n_1 \in \NN$ such that $x_{n} \in B_{\veps/2}(\xi)$,
  that is, $d(\xi, x_{n_1}) < \frac{\veps}{2}$, for all $n \geq n_1$.
  Likewise, there is $n_2 \in \NN$ with $d(\xi, x_{n}) < \frac{\veps}{2}$ for all $n \geq n_2$.
  Pick some $n$ with $n \geq \max\set{n_1,n_2}$. Then, by the triangle inequality:
  \begin{IEEEeqnarray*}{rCl"s}
    \veps = d(\xi, \xi') &\leq& d(\xi, x_n) + d(x_n, \xi') & by the triangle inequality \\
    &=& d(\xi, x_n) + d(\xi', x_n) & by symmetry \\
    &<& \tfrac{\veps}{2} + d(\xi', x_n) & since $n \geq n_1$ \\
    &<& \tfrac{\veps}{2} + \tfrac{\veps}{2} & since $n \geq n_2$ \\
    &=& \veps
  \end{IEEEeqnarray*}
  This is a contradiction, since it says $\veps < \veps$.
\end{proof}
  
